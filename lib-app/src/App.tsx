import "./App.css";
import styled from "styled-components";
import { Test, WeekdayCircles } from "@flit/lib";
import { useState } from "react";

export interface DaysOfWeekType {
  enableMonday: boolean;
  enableTuesday: boolean;
  enableWednesday: boolean;
  enableThursday: boolean;
  enableFriday: boolean;
  enableSaturday: boolean;
  enableSunday: boolean;
}

const App = () => {
  const [valueState, setValueState] = useState({
    enableMonday: false,
    enableTuesday: false,
    enableThursday: false,
    enableFriday: false,
    enableSaturday: false,
    enableSunday: false,
    enableWednesday: false,
  });

  const updateDays = (day: keyof DaysOfWeekType) => {
    const newValueState = {
      ...valueState,
      [day]: !valueState[day],
    };
    setValueState(newValueState);
  };
  return (
    <div>
      <StyledContent>test</StyledContent>
      <Test />
      <WeekdayCircles
        daysOfWeekEnabled={valueState}
        update={updateDays}
      />
    </div>
  );
};

export default App;

const StyledContent = styled.div`
  color: red;
`;
