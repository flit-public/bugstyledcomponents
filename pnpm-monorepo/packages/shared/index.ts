export { Test } from "./test";
export { Title } from "./components/Title";
export { Input } from "./components/Input";
export { Button } from "./components/Button";
export { Spacer } from "./components/Spacer";
export { Menu } from "./components/Menu";
export { TooltipComponent } from "./components/Tooltip";
export { InitialCircle } from "./components/InitialCircle";
export { colors } from "./styles/colors";

export const sayHi = (userName: string) => console.log(`Hi ${userName}`)
