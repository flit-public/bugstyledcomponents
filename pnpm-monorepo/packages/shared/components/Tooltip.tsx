import styled from "styled-components";
import { Spacer } from "./Spacer";
import { Tooltip } from "react-tooltip";

type DirectionType = "top" | "right" | "bottom" | "left";

export interface TooltipComponentProps {
  id: string;
  place: DirectionType;
  title?: string;
  content?: string;
  withoutSpacer?: boolean;
}

export const TooltipComponent = (props: TooltipComponentProps) => {
  return (
    <StyledTooltip id={props.id} place={props.place}>
      {!props.withoutSpacer && <Spacer y={1} />}
      {props.title && (
        <>
          <StyledTooltipTitle>{props.title}</StyledTooltipTitle>
          <Spacer y={0.75} />
        </>
      )}
      <StyledTooltipContent>{props.content}</StyledTooltipContent>
      {!props.withoutSpacer && <Spacer y={1} />}
    </StyledTooltip>
  );
};

const StyledTooltip = styled(Tooltip)`
  display: flex;
  flex-direction: column;
  border-radius: 0.75rem !important;
  max-width: 20rem;
  background-color: black;
  font-weight: 400;
  font-size: 1rem;
  font-family: Inter;
  font-style: normal;
  white-space: normal;
  line-height: normal;
`;

const StyledTooltipTitle = styled.div`
  font-weight: 600;
  &::first-letter {
    text-transform: uppercase;
  }
`;

const StyledTooltipContent = styled.div`
  &::first-letter {
    text-transform: uppercase;
  }
`;
