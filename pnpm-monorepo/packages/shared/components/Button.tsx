import { HTMLAttributes } from "react";
import styled from "styled-components";
import { colors } from "../styles/colors";
import { Spacer } from "./Spacer";

export interface ButtonProps extends HTMLAttributes<HTMLButtonElement> {
  title: string;
  width?: number;
  allWidth?: boolean;
  height?: number;
  type?: "submit";
  disabled?: boolean;
  withoutBackground?: boolean;
  borderColor?: string;
  Element?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  fontWeight?: number;
  backgroundColor?: string;
  borderRadius?: number;
  hasStrokeSvg?: boolean;
  underline?: boolean;
  colorHover?: string;
  backgroundColorInactive?: string;
}

export const Button = (props: ButtonProps) => {
  const {
    title,
    width,
    allWidth,
    height,
    type,
    disabled,
    withoutBackground,
    borderColor,
    Element,
    fontWeight,
    backgroundColor,
    borderRadius,
    hasStrokeSvg,
    underline,
    colorHover,
    backgroundColorInactive,
    ...buttonProps
  } = props;
  return (
    <StyledButton
      {...buttonProps}
      width={width}
      $allWidth={allWidth}
      height={height}
      type="submit"
      inactive={disabled}
      withoutBackground={withoutBackground}
      borderColor={borderColor}
      fontWeight={fontWeight}
      backgroundColor={backgroundColor}
      hasStrokeSvg={hasStrokeSvg}
      borderRadius={borderRadius}
      underline={underline}
      colorHover={colorHover}
      backgroundColorInactive={backgroundColorInactive}
    >
      {Element && (
        <>
          <Element /> <Spacer x={0.5} />
        </>
      )}
      <StyledTitle>{title}</StyledTitle>
    </StyledButton>
  );
};

interface StyledButtonArgs {
  width?: number;
  height?: number;
  inactive?: boolean;
  withoutBackground?: boolean;
  borderColor?: string;
  fontWeight?: number;
  backgroundColor?: string;
  borderRadius?: number;
  hasStrokeSvg?: boolean;
  underline?: boolean;
  allWidth?: boolean;
  colorHover?: string;
  backgroundColorInactive?: string;
}

const StyledButton = styled.button<StyledButtonArgs>`
  :hover {
    background-color: ${({ inactive, withoutBackground, colorHover }) =>
      colorHover && !inactive
        ? colorHover
        : colorHover && inactive
        ? ""
        : withoutBackground || inactive
        ? ""
        : colors.primary80};
    color: ${({ withoutBackground }) => withoutBackground && colors.disabled};
    & svg path {
      fill: ${({ withoutBackground }) =>
        withoutBackground ? colors.disabled : colors.white};
      stroke: ${({ withoutBackground, hasStrokeSvg }) =>
        hasStrokeSvg
          ? withoutBackground
            ? colors.disabled
            : colors.white
          : ""};
    }
    text-decoration: ${({ underline }) =>
      underline ? `underline ${colors.disabled}` : ""};
  }
  :active {
    background-color: ${({ inactive, withoutBackground }) =>
      withoutBackground || inactive ? "" : "colors.primary50"};
  }
  text-decoration: ${({ underline }) => (underline ? `underline` : "")};
  width: ${({ width, allWidth }) =>
    allWidth ? "100%" : width ? `${width}rem` : "10rem"};
  height: ${({ height }) => (height ? `${height}rem` : "2.5rem")};
  line-height: 2.5rem;
  cursor: ${({ inactive }) => (inactive ? "not-allowed" : "pointer")};
  border-radius: ${({ borderRadius }) =>
    borderRadius ? `${borderRadius}rem` : "0.5rem"};
  background-color: ${({
    backgroundColor,
    backgroundColorInactive,
    inactive,
    withoutBackground,
  }) =>
    backgroundColor && inactive
      ? backgroundColorInactive
      : backgroundColor
      ? backgroundColor
      : withoutBackground
      ? colors.white
      : inactive
      ? colors.primary50
      : colors.primary};
  color: ${({ withoutBackground }) =>
    withoutBackground ? colors.black : colors.white};
  font-family: Inter;
  text-align: center;
  font-weight: ${({ fontWeight }) => (fontWeight ? `${fontWeight}` : "700")};
  display: block;
  border: ${({ borderColor }) =>
    borderColor ? `2px solid ${borderColor}` : "none"};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledTitle = styled.div`
  &::first-letter {
    text-transform: uppercase;
  }
`;
