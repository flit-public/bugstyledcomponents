import { ComponentPropsWithRef, forwardRef, useState } from "react";
import styled from "styled-components";
import { colors } from "../styles/colors";
import closedEye from "../assets/closed-eye.svg";
import openEye from "../assets/open-eye.svg";
import { Spacer } from "./Spacer";

export interface InputProps extends ComponentPropsWithRef<"input"> {
  label?: string | JSX.Element | JSX.Element[];
  error?: string;
  hasSmallBottomSpacer?: boolean;
  noneBottomSpacer?: boolean;
  allWidth?: boolean;
  endText?: string;
}

export const Input = (
  (
    {
      label,
      type,
      error,
      hasSmallBottomSpacer,
      noneBottomSpacer,
      allWidth,
      endText,
      ...props
    } : InputProps
  ) => {
    const isVisible = false;
    const [isPasswordVisible, setIsPasswordVisible] = useState(isVisible);
    const [isInputFocus, setIsInputFocus] = useState<boolean>(false);
    const isPassword = type === "password";
    const inputType = type;

    const togglePasswordVisibility = () => {
      setIsPasswordVisible((isVisible) => !isVisible);
    };

    const capitalize = (str: string) => {
      return str.charAt(0).toUpperCase() + str.slice(1);
    };

    return (
      <StyledContainer $allWidth={allWidth || false}>
        {label && <StyledLabel> {label} </StyledLabel>}
        <StyledBackground
          $isError={error !== undefined}
          $isInputFocus={isInputFocus}
        >
          <StyledInputContainer
            $isError={error !== undefined}
            width={props?.width as number}
            $allWidth
            $isInputFocus={isInputFocus}
          >
            <StyledInputFlex>
              <StyledInputValue
                $isError={error !== undefined}
                {...props}
                $isPassword={true}
                type={isPasswordVisible ? "text" : inputType}
                placeholder={props.placeholder && capitalize(props.placeholder)}
                $allWidth
                onFocus={() => {
                  setIsInputFocus(true);
                }}
                onBlur={(e) => {
                  props.onBlur && props.onBlur(e);
                  setIsInputFocus(false);
                }}
              />
              {isPassword ? (
                <StyledToggleButton
                  type="button"
                  onClick={togglePasswordVisibility}
                >
                  <StyledEyeImage
                    src={isPasswordVisible ? closedEye : openEye}
                    alt="password visibility"
                  />
                  <Spacer x={0.5} />
                </StyledToggleButton>
              ) : (
                endText && (
                  <StyledEndText>
                    {endText}
                    <Spacer x={1} />
                  </StyledEndText>
                )
              )}
            </StyledInputFlex>
          </StyledInputContainer>
        </StyledBackground>
        {error && (
          <div>
            <Spacer y={0.5} />
            <StyledErrorMessage>{error}</StyledErrorMessage>
          </div>
        )}
        {hasSmallBottomSpacer ? (
          <Spacer y={1} />
        ) : (
          !noneBottomSpacer && <Spacer y={1.5} />
        )}
      </StyledContainer>
    );
  }
);

const StyledContainer = styled.div<{ $allWidth: boolean }>`
  width: ${({ $allWidth }) => ($allWidth ? "100%" : "20rem")};
`;

const StyledErrorMessage = styled.div`
  color: ${colors.alertMessage};
  font-size: 1rem;
  line-height: 1rem;
  &::first-letter {
    text-transform: uppercase;
  }
`;

const StyledLabel = styled.label`
  display: block;
  font-size: 1rem;
  font-weight: 500;
  font-family: Inter;
  color: ${colors.grey};
  border: solid 0.25rem transparent;
  &::first-letter {
    text-transform: uppercase;
  }
`;

const StyledInputFlex = styled.div`
  display: flex;
  width: 100%;
`;

const StyledInputValue = styled.input<{
  $isPassword: boolean;
  $isError: boolean;
  $allWidth: boolean;
}>`
  width: ${({ $isPassword, $width, $allWidth }) =>
    $allWidth ? "100%" : $width ? $width + "rem" : $isPassword ? "16rem" : "18rem"};
  border-radius: 0.5rem;
  border-color: ${({ $isPassword, $isError }) =>
    $isPassword ? "transparent" : $isError ? colors.alert80 : colors.ligthGrey};
  height: 3.3rem;
  font-size: 1rem;
  line-height: 100%;
  color: ${colors.grey};
  background-color: ${({ $isPassword, $isError }) =>
    $isPassword ? "transparent" : $isError ? colors.alert20 : colors.white};
  &:focus {
    outline: none;
    background-size: 100% 2px, 100% 1px;
  }
  padding: 0;
  padding-left: 1rem;
  padding-right: 1rem;
`;

const StyledInputContainer = styled.div<{
  $allWidth: boolean;
  $isError: boolean;
  $width: number;
  $isInputFocus: boolean;
}>`
  display: flex;
  width: ${({ $width, $allWidth }) =>
    $allWidth ? "100%" : $width ? $width + "rem" : "20rem"};
  border: solid 1px;
  border-radius: 0.5rem;
  border-color: ${({ $isError }) =>
    $isError ? colors.alert80 : colors.ligthGrey};
  background-color: ${({ $isError }) =>
    $isError ? colors.alert20 : colors.white};
  outline: none;
  &:focus {
    outline: none;
    background-size: 100% 2px, 100% 1px;
  }
  border-color: ${({ $isError, $isInputFocus }) =>
    !$isError && $isInputFocus && colors.primary};
  input::-webkit-inner-spin-button,
  input::-webkit-outer-spin-button { 
    -webkit-appearance: none;
    margin:0;
`;

const StyledBackground = styled.div<{
  isError: boolean;
  isInputFocus: boolean;
}>`
  display: flex;
  transition: all 0.4s ease-out;
  border: solid 0.25rem;
  border-color: transparent;
  border-radius: 0.75rem;
  border-color: ${({ isError, isInputFocus }) =>
    !isError && isInputFocus && colors.primary33};
  &:hover {
    transition: all 0.4s ease-out;
    border-color: ${({ isError }) => !isError && colors.primary33};
  }
`;

const StyledToggleButton = styled.button`
  cursor: pointer;
  border: none;
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 2.75rem;
`;

const StyledEyeImage = styled.img`
  width: 1rem;
  height: 1rem;
`;

const StyledEndText = styled.div`
  display: flex;
  background: transparent;
  align-items: center;
  justify-content: flex-end;
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 1rem;
  color: ${colors.contentSecondary};
`;
