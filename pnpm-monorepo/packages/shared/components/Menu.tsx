import { useState } from "react";
import styled from "styled-components";
import logo from "../assets/logo.svg";
import enlargedLogo from "../assets/logoFlit.svg";
import { colors } from "../styles/colors";
import { Spacer } from "./Spacer";
import rightArrow from "../assets/doubleArrowRightSolid.svg";
import rightArrowHover from "../assets/doubleArrowRightWhite.svg";
import leftArrow from "../assets/doubleArrowLeftSolid.svg";
import badge from "../assets/badge.svg";
import leftArrowHover from "../assets/doubleArrowLeftWhite.svg";
import { TooltipComponent } from "./Tooltip";
import { useTranslation } from "react-i18next";
//import { ReactComponent as Logout } from "../assets/logoutIcon.svg";
import { InitialCircle } from "./InitialCircle";

export interface MenuProps {
  sections: Array<{
    route: string;
    iconSrc: JSX.Element;
    title: string;
    newCreation?: boolean;
  }>;
  selectedRoute?: string;
  manager?: {
    firstName: string;
    lastName: string;
    email: string;
    uuid: string;
    companyUuid: string;
    hasBeenWelcomed: boolean;
  } | null;
  onChangeRoute: (route: string) => void;
  route: string;
  logout: () => void;
}

interface StyledHeaderLogoProps {
  selected: boolean;
  isMenuEnlarged: boolean;
}

export interface HomeTrailerProps {
  route: string;
  selectedRoute?: string;
  onChangeRoute: (route: string) => void;
  manager?: {
    firstName: string;
    lastName: string;
    email: string;
    uuid: string;
    companyUuid: string;
    hasBeenWelcomed: boolean;
  } | null;
  isMenuEnlarged: boolean;
  logout: () => void;
}
interface HomeInitialCircleProps {
  selected?: boolean;
}

const MenuHeaderLogo = (props: {
  isArrowHover: boolean;
  setIsArrowHover: (value: boolean) => void;
  isMenuHover: boolean;
  setIsMenuHover: (value: boolean) => void;
  isMenuEnlarged: boolean;
  setIsMenuEnlarged: (value: boolean) => void;
  setStateByDefault: () => void;
}) => {
  const { t } = useTranslation();

  return (
    <StyledHeaderLogo>
      <Spacer x={1} />
      {props.isMenuEnlarged ? (
        <>
          <StyledImg src={enlargedLogo} alt="logo" />
          <StyledArrow isMenuEnlarged={true}>
            {props.isMenuHover &&
              (props.isArrowHover ? (
                <div onMouseLeave={() => props.setIsArrowHover(false)}>
                  <StyledImg
                    src={leftArrowHover}
                    alt="leftArrowHover"
                    onClick={() => {
                      props.setIsMenuEnlarged(false);
                      props.setStateByDefault();
                    }}
                    data-tooltip-id="TooltipClose"
                  />
                  <TooltipComponent
                    id={"TooltipClose"}
                    place={"left"}
                    content={t("menu.tooltip.close") || ""}
                    withoutSpacer
                  />
                </div>
              ) : (
                <div onMouseEnter={() => props.setIsArrowHover(true)}>
                  <StyledImg src={leftArrow} alt="leftArrow" />
                </div>
              ))}
          </StyledArrow>
        </>
      ) : (
        <StyledArrow isMenuEnlarged={false}>
          {props.isMenuHover ? (
            props.isArrowHover ? (
              <div onMouseLeave={() => props.setIsArrowHover(false)}>
                <StyledImg
                  src={rightArrowHover}
                  alt="rightArrowHover"
                  onClick={() => {
                    props.setIsMenuEnlarged(true);
                    props.setStateByDefault();
                  }}
                  data-tooltip-id="TooltipOpen"
                />
                <TooltipComponent
                  id={"TooltipOpen"}
                  place={"right"}
                  content={t("menu.tooltip.open") || ""}
                  withoutSpacer
                />
              </div>
            ) : (
              <div onMouseEnter={() => props.setIsArrowHover(true)}>
                <StyledImg src={rightArrow} alt="rightArrow" />
              </div>
            )
          ) : (
            <StyledImg src={logo} alt="logo" />
          )}
        </StyledArrow>
      )}
      <Spacer x={1} />
    </StyledHeaderLogo>
  );
};

export const HomeTrailer = (props: HomeTrailerProps) => {
  const { t } = useTranslation();

  return (
    <StyledTrailerContainer isMenuEnlarged={props.isMenuEnlarged}>
      <StyledLogout
        isMenuEnlarged={props.isMenuEnlarged}
        onClick={() => {
          props.logout();
        }}
      >
        {props.isMenuEnlarged ? (
          <>
            <Spacer x={1.7} />
            <Spacer x={1} />
            <StyledUppercase>{t("home.menu.logout")}</StyledUppercase>
          </>
        ) : (''
        )}
      </StyledLogout>
      <Spacer y={2} />
      {props.manager && (
        <StyledManagerContainer
          selected={props.selectedRoute === props.route}
          onClick={() => props.onChangeRoute(props.route)}
        >
          {props.isMenuEnlarged ? (
            <>
              <Spacer x={1} />
              <InitialCircle
                letter={props.manager.firstName[0]}
                size="small"
                clickable={true}
              />
              <Spacer x={1} />
              <StyledManager>
                {props.manager.firstName + " " + props.manager.lastName}
                <br />
                <StyledManagerContent>
                  {t("home.menu.manager")}
                </StyledManagerContent>
              </StyledManager>
              <Spacer x={1} />
            </>
          ) : (
            <>
              <Spacer x={1} />
              <InitialCircle
                letter={props.manager.firstName[0]}
                size="small"
                clickable={true}
              />
              <Spacer x={1} />
            </>
          )}
        </StyledManagerContainer>
      )}
    </StyledTrailerContainer>
  );
};

export const Menu = (props: MenuProps) => {
  const [isArrowHover, setIsArrowHover] = useState(false);
  const [isMenuHover, setIsMenuHover] = useState(false);
  const [isMenuEnlarged, setIsMenuEnlarged] = useState(false);
  const setStateByDefault = () => {
    setIsMenuHover(false);
    setIsArrowHover(false);
  };

  return (
    <>
      <Spacer x={1.5} />
      <StyledContainer
        onMouseEnter={() => setIsMenuHover(true)}
        onMouseLeave={() => setStateByDefault()}
        isMenuEnlarged={isMenuEnlarged}
      >
        <Spacer y={2} />

        <MenuHeaderLogo
          isArrowHover={isArrowHover}
          setIsArrowHover={(value) => setIsArrowHover(value)}
          isMenuHover={isMenuHover}
          setIsMenuHover={(value) => setIsMenuHover(value)}
          isMenuEnlarged={isMenuEnlarged}
          setIsMenuEnlarged={(value) => setIsMenuEnlarged(value)}
          setStateByDefault={() => setStateByDefault()}
        />

        <Spacer y={2} />
        {props.sections.map((section) => (
          <StyledSectionContainer
            key={section.route}
            selected={section.route === props.selectedRoute}
            onClick={() => props.onChangeRoute(section.route)}
            isMenuEnlarged={isMenuEnlarged}
          >
            {isMenuEnlarged ? <Spacer x={1.6} /> : <Spacer x={1} />}
            <>
              {section.newCreation && (
                <StyledBadge isMenuEnlarged={isMenuEnlarged}>
                  <StyledImg src={badge} alt={badge} />
                </StyledBadge>
              )}
              <StyledIcon>{section.iconSrc}</StyledIcon>
            </>
            <StyledText isMenuEnlarged={isMenuEnlarged}>
              <Spacer x={1} />
              <StyledUppercase>{section.title}</StyledUppercase>
            </StyledText>
            <Spacer x={1} />
          </StyledSectionContainer>
        ))}
        <Spacer y={1} />
        <StyledTrailer isMenuEnlarged={isMenuEnlarged}>
          <HomeTrailer
            route={props.route}
            onChangeRoute={props.onChangeRoute}
            selectedRoute={props.selectedRoute}
            manager={props.manager}
            isMenuEnlarged={isMenuEnlarged}
            logout={props.logout}
          />
        </StyledTrailer>
        <Spacer y={2.5} />
      </StyledContainer>
      <Spacer x={1.5} />
    </>
  );
};

const StyledTrailerContainer = styled.div<{ isMenuEnlarged: boolean }>`
  display: flex;
  flex-direction: column;
  align-items: ${({ isMenuEnlarged }) =>
    isMenuEnlarged ? "flex-start" : "center"};
`;

const StyledLogout = styled.div<{ isMenuEnlarged: boolean }>`
  display: flex;
  flex-direction: row;
  cursor: pointer;
  :hover {
    color: ${colors.disabled};
    & svg path {
      fill: ${colors.disabled};
    }
  }
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "100%" : "")};
`;

const StyledManager = styled.div`
  display: flex;
  flex-direction: column;
  width: 10rem;
`;

const StyledManagerContent = styled.div`
  font-size: 0.875rem;
  color: ${colors.disabled};

  &::first-letter {
    text-transform: uppercase;
  }
`;

const StyledManagerContainer = styled.div<HomeInitialCircleProps>`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 4.5rem;
  width: 100%;
  line-height: 1.5rem;
  border-radius: 0.75rem;
  background-color: ${({ selected }) => (selected ? colors.white : "none")};
  justify-content: center;
  font-size: 1rem;
  font-family: Inter;
  font-style: normal;
  font-weight: 400;
  cursor: pointer;
  :hover {
    background-color: ${colors.white};
  }
`;

const StyledText = styled.div<{ isMenuEnlarged: boolean }>`
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "100%" : "0px")};
  display: flex;
  font-size: 1rem;
  font-family: Inter;
  font-style: normal;
`;

const StyledUppercase = styled.div`
  &::first-letter {
    text-transform: uppercase;
  }
`;

const StyledTrailer = styled.div<{ isMenuEnlarged: boolean }>`
  display: flex;
  justify-content: center;
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "15.5rem" : "4.5rem")};
  margin-top: auto;
`;

const StyledContainer = styled.div<{ isMenuEnlarged: boolean }>`
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "15.5rem" : "4.5rem")};
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  height: 100%;
  transition: all 0.5s ease;
`;

const StyledImg = styled.img``;

const StyledIcon = styled.div`
  min-width: 1.3rem;
`;

const StyledBadge = styled.div<{ isMenuEnlarged: boolean }>`
  position: absolute;
  display: flex;
  align-items: flex-start;
  justify-content: ${({ isMenuEnlarged }) =>
    isMenuEnlarged ? "flex-start" : "flex-end"};
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "10.75rem" : "2.25rem")};
  height: 2.25rem;
`;

const StyledSectionContainer = styled.div<StyledHeaderLogoProps>`
  width: ${({ isMenuEnlarged }) => (isMenuEnlarged ? "15.5rem" : "4.5rem")};
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3.5rem;
  border-radius: 1rem;
  background-color: ${({ selected }) => (selected ? colors.paleGreen : "none")};
  font-weight: ${({ selected }) => (selected ? "600" : "500")};
  cursor: pointer;
  div {
    overflow: hidden;
  }
  transition: ${({ isMenuEnlarged }) =>
    isMenuEnlarged ? "none" : "all 0.5s ease"};
  &:hover {
    color: ${colors.disabled};
    & svg path {
      fill: ${colors.disabled};
    }
  }
`;

const StyledHeaderLogo = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: 3.5rem;
  border-radius: 1rem;
`;

const StyledArrow = styled.div<{ isMenuEnlarged: boolean }>`
  display: flex;
  width: 100%;
  justify-content: ${({ isMenuEnlarged }) =>
    isMenuEnlarged ? "flex-end" : "center"};
  align-items: center;
  cursor: pointer;
`;
