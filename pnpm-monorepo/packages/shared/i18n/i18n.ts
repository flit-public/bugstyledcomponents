import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationFrench from "./translations/french.json";
const resources = {
  fr: { translation: translationFrench },
};
console.log("michel");
console.log(resources.fr.translation);
i18n
  .use(initReactI18next) // bind react-i18next to the instance
  .init({
    lng: "fr",
    fallbackLng: "fr",
    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react!!
    },
    resources,

    // react i18next special options (optional)
    // override if needed - omit if ok with defaults
    /*
    react: {
      bindI18n: 'languageChanged',
      bindI18nStore: '',
      transEmptyNodeValue: '',
      transSupportBasicHtmlNodes: true,
      transKeepBasicHtmlNodesFor: ['br', 'strong', 'i'],
      useSuspense: true,
    }
    */
  });

console.log("loaded");
export default i18n;
