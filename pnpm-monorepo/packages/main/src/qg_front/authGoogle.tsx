
import { Button, Input, Title } from "@pnpm-monorepo/shared";
import styled from "styled-components";

// QG BACK : TODO : check token for each requests

// https://developers.google.com/identity/gsi/web/guides/verify-google-id-token?hl=fr
// GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
//     // Specify the CLIENT_ID of the app that accesses the backend:
//     .setAudience(Collections.singletonList(CLIENT_ID))
//     // Or, if multiple clients access the backend:
//     //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
//     .build();
// GoogleIdToken idToken = verifier.verify(idTokenString);
// if (idToken != null) {

export const AuthGoogle = () => {
  return (
    <StyledContainer>
      <Title text="click"/>
      <StyledButton title="click" underline/>
      <Input />
      <Input type="password" />
      test
    </StyledContainer>
  );
};

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vh;
`;

const StyledButton = styled(Button)`
  color: red;
`;
