import "./App.css";
import "./i18n/i18n";
import { routes } from "./routes/router";
import { useRoutes } from "react-router-dom";


// const App = () => {
//   // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-call
//   //console.log(sayHi('Marion'))
//  // return (
//     // <div>
//     // <Title text="test title"/>
//     // <Button title="Click"/>
//     // test
//     // </div>
    
//     //)
//     return useRoutes(routes);
// };

const App = () => {
  return useRoutes(routes);
};

export default App;