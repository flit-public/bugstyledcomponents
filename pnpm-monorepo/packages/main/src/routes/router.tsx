import { RouteObject } from "react-router-dom";
import { AuthGoogle } from "../qg_front/authGoogle";

export const routes: RouteObject[] = [
  {
    path: "/login",
    element: <AuthGoogle />,
  }
];
