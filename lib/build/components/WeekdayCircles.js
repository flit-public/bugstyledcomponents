"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeekdayCircles = exports.weekLetter = void 0;
const jsx_runtime_1 = require("react/jsx-runtime");
const styled_components_1 = __importDefault(require("styled-components"));
const colors_1 = require("../styles/colors");
const weekToLetter = {
    enableMonday: "L",
    enableTuesday: "M",
    enableWednesday: "M",
    enableThursday: "J",
    enableFriday: "V",
    enableSaturday: "S",
    enableSunday: "D",
};
const weekLetter = (day) => {
    return weekToLetter[day];
};
exports.weekLetter = weekLetter;
const WeekdayCircles = (props) => {
    const { daysOfWeekEnabled } = props;
    return ((0, jsx_runtime_1.jsx)(StyledContainer, { children: Object.keys(daysOfWeekEnabled).map((key, index) => ((0, jsx_runtime_1.jsx)(StyledCircle, { inactive: props.daysOfWeekEnabled[key], onClick: () => {
                props.update(key);
            }, children: (0, exports.weekLetter)(key) }, index))) }));
};
exports.WeekdayCircles = WeekdayCircles;
const StyledCircle = styled_components_1.default.div `
  background-color: ${({ inactive }) => inactive ? colors_1.colors.primary : colors_1.colors.middleGrey};
  width: 2rem;
  height: 2rem;
  border-radius: 1.25rem;
  color: white;
  line-height: 2rem;
  text-align: center;
  font-family: inter;
  font-size: 1rem;
  cursor: pointer;
  align-self: flex-start;
`;
const StyledContainer = styled_components_1.default.div `
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-self: flex-start;
  width: 17rem;
`;
