interface WeekdayCirclesProps {
    daysOfWeekEnabled: DaysOfWeekType;
    update: (day: keyof DaysOfWeekType) => void;
}
interface DaysOfWeekType {
    enableMonday: boolean;
    enableTuesday: boolean;
    enableWednesday: boolean;
    enableThursday: boolean;
    enableFriday: boolean;
    enableSaturday: boolean;
    enableSunday: boolean;
}
export declare const weekLetter: (day: keyof DaysOfWeekType) => string;
export declare const WeekdayCircles: (props: WeekdayCirclesProps) => import("react/jsx-runtime").JSX.Element;
export {};
//# sourceMappingURL=WeekdayCircles.d.ts.map