"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.colors = void 0;
exports.colors = {
    alert20: "#FE949420",
    alert80: "#FE949480",
    alertMessage: "#EB4A4A",
    tertiary: "#FEE58A",
    tertiary33: "#FEE58A33",
    primary: "#3CD3AD",
    primary33: "#3DCDAD33",
    primary50: "#3DCDAD50",
    primary80: "#3DCDAD80",
    paleGreen: "#B8EADD",
    ligthGrey: "#B4BEC054",
    grey: "#535654",
    middleGrey: "#D9D9D9",
    black: "#333333",
    white: "#FFFFFF",
    background: "#F5F8F7",
    silver: "#b4bec040",
    contentSecondary: "#535654",
    contentTertiary: "#757779",
    contentTertiary33: "#75777933",
    disabled: "#8F8F8F",
    red: "#E7625C",
};
