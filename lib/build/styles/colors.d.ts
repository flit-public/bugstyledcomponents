export declare const colors: {
    alert20: string;
    alert80: string;
    alertMessage: string;
    tertiary: string;
    tertiary33: string;
    primary: string;
    primary33: string;
    primary50: string;
    primary80: string;
    paleGreen: string;
    ligthGrey: string;
    grey: string;
    middleGrey: string;
    black: string;
    white: string;
    background: string;
    silver: string;
    contentSecondary: string;
    contentTertiary: string;
    contentTertiary33: string;
    disabled: string;
    red: string;
};
//# sourceMappingURL=colors.d.ts.map