"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Test = void 0;
const jsx_runtime_1 = require("react/jsx-runtime");
const styled_components_1 = __importDefault(require("styled-components"));
const Test = () => {
    return (0, jsx_runtime_1.jsx)(StyledDiv, { children: "Coucou" });
};
exports.Test = Test;
const StyledDiv = styled_components_1.default.div `
  color: blue;
`;
